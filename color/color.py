

class Color:

    COLORS = {"black" : "30",
              "red": "31",
              "green": "32",
              "yellow": "33",
              "blue" : "34",
              "magenta" : "35",
              "cyan" : "36",
              "white" : "37"}

    STYLES = {"bold" : "1",
              "blink" : "5"}

    @staticmethod
    def print(text, fg_col=None, bg_col=None, style=None, end="\n"):
        if style is not None:
            print("\033[;" + Color.STYLES[style] + "m", end="")
        if bg_col is not None and fg_col is not None:
            print("\033[" + Color.COLORS[fg_col] + ";" + str(int(Color.COLORS[bg_col]) + 10) + "m", end="")
        elif fg_col is not None:
            print("\033[" + Color.COLORS[fg_col] + "m", end="")
        elif bg_col is not None:
            print("\033[;" + str(int(Color.COLORS[bg_col]) + 10) + "m", end="")

        print(text, end="")

        print("\033[0;0m", end=end)