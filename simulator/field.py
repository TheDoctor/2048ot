

class Field:

    def __init__(self, val):
        self._set_val(val)
        self.done = False
        self.new = True

    def reset(self):
        self.done = False

    def simulated(self):
        return self.done

    def is_empty(self):
        return self.val == 0

    def double(self):
        self._set_val(self.val * 2)
        self.done = True

    def _set_val(self, val):
        self.val = val

        # Color
        self.fg_col = "white"
        if val == 0:
            self.bg_col = None
            self.fg_col = None
        if val == 2:
            self.bg_col = "green"
        elif val == 4:
            self.bg_col = "yellow"
        elif val == 8:
            self.bg_col = "magenta"
        elif val == 16:
            self.bg_col = "cyan"
        elif val == 32:
            self.bg_col = "blue"
        elif val >= 64:
            self.bg_col = "red"
        #else:
        #    self.bg_col = "black"
        #    self.fg_col = "white"

    def __eq__(self, other):
        return self.val == other.val