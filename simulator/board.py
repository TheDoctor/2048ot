from random import randint

from simulator.field import Field
from color.color import Color


class Board:

    # Settings
    FIELD_WIDTH = 8
    FIELD_PAD = 2
    POS_MOVES = ["w", "a", "s", "d"]

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.points = 0
        self.board = [[Field(0) for _ in range(width)] for _ in range(height)]

    def is_empty(self, l, c):
        return self.board[l][c].is_empty()

    def is_lost(self):
        for l in range(self.height):
            for c in range(self.width):
                if self.is_empty(l, c):
                    return False

        # No more move possible?
        for m in self.POS_MOVES:
            backup = self.save_board()
            if self.move(m):
                self.load_board(backup)
                return False

        return True

    def move(self, dir):
        # Set unchanged
        for l in self.board:
            for c in l:
                c.reset()

        # Something changed?
        b_dirty = False

        # If top/down swipe
        if dir == "w" or dir == "s":
            for l in range(self.height - 1):
                for c in range(self.width):
                    cur_l = l + 1 if dir == "w" else (self.height - l - 2)
                    next_l = cur_l - 1 if dir == "w" else cur_l + 1
                    while not (next_l <= -1 or next_l >= self.height):
                        if self.next_step_for_field(cur_l, next_l, c, c):
                            b_dirty = True
                        cur_l = cur_l - 1 if dir == "w" else cur_l + 1
                        next_l = next_l - 1 if dir == "w" else next_l + 1

        # If left/right swipe
        elif dir == "a" or dir == "d":
            for c in range(self.width - 1):
                for l in range(self.height):
                    cur_c = c + 1 if dir == "a" else (self.width - c - 2)
                    next_c = cur_c - 1 if dir == "a" else cur_c + 1
                    while not (next_c <= -1 or next_c >= self.width):
                        if self.next_step_for_field(l, l, cur_c, next_c):
                            b_dirty = True
                        cur_c = cur_c - 1 if dir == "a" else cur_c + 1
                        next_c = next_c - 1 if dir == "a" else next_c + 1

        return b_dirty

    def set_field(self, l, c, f):
        self.board[l][c] = f

    def save_board(self):
        backup = [[Field(f.val) for f in l] for l in self.board]
        return backup, self.points

    def load_board(self, backup):
        self.board = backup[0]
        self.points = backup[1]

    def generate_rand_field(self):
        empty_fields = []
        for l in range(self.height):
            for c in range(self.width):
                if self.is_empty(l, c):
                    empty_fields.append((l, c))

        rand_field = empty_fields[randint(0, len(empty_fields) - 1)]
        self.board[rand_field[0]][rand_field[1]] = Field(2)

    def next_step_for_field(self, cur_l, next_l, cur_c, next_c):
        if not self.is_empty(cur_l, cur_c) and not self.board[cur_l][cur_c].simulated():
            if self.is_empty(next_l, next_c):
                self.board[next_l][next_c] = self.board[cur_l][cur_c]
                self.board[cur_l][cur_c] = Field(0)
                return True
            elif self.board[next_l][next_c] == self.board[cur_l][cur_c]:
                self.board[next_l][next_c].double()
                self.board[cur_l][cur_c] = Field(0)
                self.points += self.board[next_l][next_c].val
                return True
        return False

    def print_board(self):
        for l in self.board:
            # Print top line of fields
            self._print_empty_field_line(l)

            # Print line containing values
            for f in l:
                if f.val != 0:
                    f_str = str(f.val)
                    f_len = len(f_str)
                    pad_len = int((self.FIELD_WIDTH - f_len) / 2)
                    f_str = " " * pad_len + f_str + " " * pad_len

                    if pad_len * 2 + f_len != self.FIELD_WIDTH:
                        f_str += " "

                    Color.print(f_str, style="bold", fg_col=f.fg_col, bg_col=f.bg_col, end=" " * self.FIELD_PAD)
                else:
                    Color.print(" " * self.FIELD_WIDTH, style="bold", fg_col=f.fg_col, bg_col=f.bg_col, end=" " * self.FIELD_PAD)
            print()

            # Print bottom line of fields
            self._print_empty_field_line(l)

            # Pad line
            print()

    def _print_empty_field_line(self, l):
        for f in l:
            Color.print(" " * self.FIELD_WIDTH, style="bold", fg_col=f.fg_col, bg_col=f.bg_col, end=" " * self.FIELD_PAD)
        print()
