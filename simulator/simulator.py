import getch

from random import randint
from time import sleep
from os import system

from simulator.board import Board
from bots.chess_bot import ChessBot


class Simulator:

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.board = Board(width, height)
        self.bot = ChessBot(3)

    # Main game loop
    def simulate(self, headless, bot, death_mode):
        current_step = 0
        step_completed = True

        if not death_mode:
            last_turn = None

        while True:
            if step_completed:
                self.board.generate_rand_field()
                step_completed = False
                current_step += 1

            if not headless:
                self.clear_screen()
                text_width = self.board.width * (self.board.FIELD_WIDTH + 1) + self.board.width - 2
                title = "| 2048ot |"
                title_deco_times = int((text_width - len(title)) / 2 / 2)
                print(title_deco_times * "-=" + title + "=-" * title_deco_times + "\n")
                self.board.print_board()
                points_text = "Points: " + str(self.board.points)
                step_text = "Step: " + str(current_step)
                print(points_text + (text_width - len(points_text) - len(step_text)) * " " + step_text)

            # Is user playing?
            if bot is None:
                user_input = self.get_input()

                if not death_mode:
                    if user_input == "l":
                        self.board.load_board(last_turn)
                    else:
                        last_turn = self.board.save_board()
            else:
                user_input = self.bot.sim_step(self.board)
                #sleep(0.5)

            # Exit
            if user_input == "#":
                exit(0)

            # Movement
            if user_input in ["w", "a", "s", "d"]:
                step_completed = self.board.move(user_input)

            if self.board.is_lost():
                # TODO: Add lost case
                exit(0)

    def get_input(self):
        c = getch.getch()

        # Exit
        if ord(c) == 3:
            return "#"

        # Arrow keys
        if ord(c) == 27:
            getch.getch()
            arrow = ord(getch.getch())

            if arrow == 65:
                return "w"
            if arrow == 66:
                return "s"
            if arrow == 67:
                return "d"
            if arrow == 68:
                return "a"
        return c

    def clear_screen(self):
        system("clear")
