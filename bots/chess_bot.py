from simulator.field import Field


class ChessBot:

    POS_MOVES = ["w", "a", "s", "d"]

    def __init__(self, depth):
        self.depth = depth

    def sim_step(self, board):
        self.board = board

        saved_depth = self.depth

        best_move = None
        best_points = -1
        while best_move is None and self.depth > -1:
            for m in self.POS_MOVES:
                backup = self.board.save_board()
                move_points = self.rec_magic(0, m)
                self.board.load_board(backup)
                if move_points > best_points:
                    best_move = m
                    best_points = move_points
            self.depth -= 1

        self.depth = saved_depth

        return best_move if best_move is not None else "w"

    def rec_magic(self, step, move):
        if self.board.move(move):
            if not self.board.is_lost():
                if step == self.depth:
                    return self.board.points
                else:
                    for l in range(self.board.height):
                        for c in range(self.board.width):
                            if self.board.is_empty(l, c):
                                worst_case = None
                                for m in self.POS_MOVES:
                                    backup = self.board.save_board()
                                    self.board.set_field(l, c, Field(2))
                                    cur_points = self.rec_magic(step + 1, m)
                                    if (worst_case is None or cur_points < worst_case) and cur_points > -1:
                                        worst_case = cur_points
                                    self.board.load_board(backup)

                                return worst_case if worst_case is not None else -1

        return -1
