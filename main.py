import argparse as ap
import argcomplete

from simulator.simulator import Simulator


if __name__ == "__main__":
    parser = ap.ArgumentParser()

    parser.add_argument("--headless", action="store_true", help="removes output")
    parser.add_argument("-s", help="specify size e.g. 4x4")
    parser.add_argument("-b", help="specify bot to use")
    parser.add_argument("-d", action="store_true", help="enables death mode (No going back)")

    args = parser.parse_args()

    # Size
    width = 4
    height = 4
    if args.s is not None:
        width = int(args.s.split('x')[0])
        height = int(args.s.split('x')[1])

    sim = Simulator(width, height)
    sim.simulate(args.headless, args.b, args.d)
